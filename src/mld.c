/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2022 Brett Sheffield <bacs@librecast.net> */
#define _GNU_SOURCE /* required for struct in6_pktinfo */
#include "mld_pvt.h"
#include "log.h"
#include <arpa/inet.h>
#include <errno.h>
#include <ifaddrs.h>
#include <poll.h>
#include <librecast/types.h>
#include <librecast/crypto.h>
#include <net/if.h>
#include <netdb.h>
#include <netinet/icmp6.h>
#include <netinet/in.h>
#include <netinet/ip6.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

static volatile int cont = 1;

/* extract interface number from ancillary control data */
static unsigned int interface_index(struct msghdr *msg)
{
	struct cmsghdr *cmsg;
	struct in6_pktinfo pi = {0};
	unsigned int ifx = 0;
	for (cmsg = CMSG_FIRSTHDR(msg); cmsg; cmsg = CMSG_NXTHDR(msg, cmsg)) {
		if (cmsg->cmsg_type == IPV6_PKTINFO) {
			/* may not be aligned, copy */
			memcpy(&pi, CMSG_DATA(cmsg), sizeof pi);
			ifx = pi.ipi6_ifindex;
			break;
		}
	}
	assert(ifx);
	return ifx;
}

unsigned int mld_idx_iface(mld_t *mld, unsigned int idx)
{
	for (int i = 0; i < mld->len; i++) {
		if (mld->ifx[i] == idx) return i;
	}
	return 0;
}

void mld_free(mld_t *mld)
{
	if(!mld) return;
	sem_destroy(&mld->sem_mld);
	job_queue_destroy(mld->timerq);
	lc_ctx_free(mld->lctx);
	free(mld);
}

void mld_stop(mld_t *mld)
{
	if(!mld) return;
	*(mld->cont) = 0;
	if (mld->sock) {
		struct ipv6_mreq req = {0};
		for (int i = 0; i < mld->len; i++) {
			if (inet_pton(AF_INET6, MLD2_CAPABLE_ROUTERS, &(req.ipv6mr_multiaddr)) != 1)
				continue;
			if (!(req.ipv6mr_interface = mld->ifx[i]))
				continue;
			setsockopt(mld->sock, IPPROTO_IPV6, IPV6_DROP_MEMBERSHIP, &req, sizeof(req));
		}
		close(mld->sock);
	}
}

static void mld_query_send(mld_t *mld, unsigned int iface, struct in6_addr *saddr)
{
	static struct timespec lastquery = {0}, now;
	struct sockaddr_in6 dst = { .sin6_family = AF_INET6 };
	struct iovec iov[1];
	struct msghdr msgh = {0};
	struct cmsghdr *cmsgh;
	size_t cmsglen = 0;
	void * databufp = NULL, *cmsgbuf, *extbuf;
	uint16_t racode = 0;
	mld_query_msg_t msg = {
		.type = 130,
		.mrc = htobe16(MLD2_QRI),
		.qrv = MLD2_ROBUSTNESS,
		.qqic = MLD2_QI
	};
	int currentlen, extlen;
	clock_gettime(CLOCK_REALTIME, &now);
	if (now.tv_sec - lastquery.tv_sec < MLD_TIMER_INTERVAL) return;

	DEBUG("sending MLD Query");
	memcpy(&msg.addr, saddr, sizeof(struct in6_addr));
	memcpy(&dst.sin6_addr, saddr, sizeof(struct in6_addr));

	/* set IP6OPT_ROUTER_ALERT (RFC 2711) */
	if ((extlen = inet6_opt_init(NULL, 0)) == -1) {
		ERROR("inet6_opt_init failed");
	}
	assert(extlen > 0);
	if ((extlen = inet6_opt_append(NULL, 0, extlen, IP6OPT_ROUTER_ALERT, 2, 2, NULL)) == -1) {
		ERROR("inet6_opt_append failed");
	}
	assert(extlen > 0);
	if ((extlen = inet6_opt_finish(NULL, 0, extlen)) == -1) {
		ERROR("inet6_opt_finish failed");
	}
	cmsglen = CMSG_SPACE(extlen);
	cmsgbuf = calloc(1, cmsglen);
	msgh.msg_name = &dst;
	msgh.msg_namelen = sizeof dst;
	msgh.msg_iov = iov;
	msgh.msg_iovlen = 1;
	msgh.msg_control = cmsgbuf;
	msgh.msg_controllen = cmsglen;
	iov[0].iov_base = &msg;
	iov[0].iov_len = sizeof msg;
	cmsgh = CMSG_FIRSTHDR(&msgh);
	assert(cmsgh);
	cmsgh->cmsg_len = CMSG_LEN(extlen);
	cmsgh->cmsg_level = IPPROTO_IPV6;
	cmsgh->cmsg_type = IPV6_HOPOPTS;
	extbuf = CMSG_DATA(cmsgh);
	if ((currentlen = inet6_opt_init(extbuf, extlen)) == -1) {
		ERROR("inet6_opt_init");
	}
	assert(currentlen > 0);
	if ((currentlen = inet6_opt_append(extbuf, extlen, currentlen,
		IP6OPT_ROUTER_ALERT, 2, 2, &databufp)) == -1)
	{
		ERROR("inet6_opt_append");
	}
	assert(currentlen > 0);
	inet6_opt_set_val(databufp, 0, &racode, sizeof(racode));
	if (inet6_opt_finish(extbuf, extlen, currentlen) == -1) {
		ERROR("inet6_opt_finish");
	}

	unsigned int ifx = mld->ifx[iface];
	if (setsockopt(mld->sock, IPPROTO_IPV6, IPV6_MULTICAST_IF, &ifx, sizeof(ifx)) == -1)
		perror("setsockopt");
	for (int i = 0; i < MLD2_ROBUSTNESS; i++)
		sendmsg(mld->sock, &msgh, 0);
	clock_gettime(CLOCK_REALTIME, &lastquery);
	free(cmsgbuf);
}

/* wait on a specific address */
int mld_wait(mld_t *mld, unsigned int ifx, struct in6_addr *addr, int flags)
{
#ifdef MLD_DEBUG
	char straddr[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, addr, straddr, INET6_ADDRSTRLEN);
	//DEBUG("%s(ifx=%u): %s", __func__, ifx, straddr);
#endif
	unsigned int iface = mld_idx_iface(mld, ifx);
	if (mld_list_grp_cmp(mld, iface, addr)) {
		//DEBUG("%s() - no need to wait - filter has address", __func__);
		return 0;
	}
	if (flags &= MLD_DONTWAIT == MLD_DONTWAIT) {
		errno = EWOULDBLOCK;
		return -1;
	}

	struct timespec ts;
	do {
		mld_query_send(mld, iface, addr);
		clock_gettime(CLOCK_REALTIME, &ts);
		ts.tv_sec++;
		sem_timedwait(&mld->sem_mld, &ts);
	}
	while (*(mld->cont) && !mld_list_grp_cmp(mld, iface, addr));

	return 0;
}

int mld_list_grp_cmp(mld_t *mld, unsigned int iface, struct in6_addr *addr)
{
	mld_grp_list_t *g, *p = NULL;
	for (g = mld->grps[iface]; g; p = g, g = g->next) {
		if (!memcmp(&g->addr, addr, sizeof(struct in6_addr))) {
			struct timespec ts;
			clock_gettime(CLOCK_REALTIME, &ts);
			if (g->expires.tv_sec < ts.tv_sec) {
				DEBUG("expired, deleting");
				/* expired, delete */
				if (p) {
					p->next = g->next;
				}
				if (g == mld->grps[iface]) {
					mld->grps[iface] = NULL;
				}
				free(g);
				return 0;
			}
			/* group is about to expire, send Query */
			else if (g->expires.tv_sec - ts.tv_sec <= MLD2_QRI / 1000)
				mld_query_send(mld, iface, addr);
			return -1;
		}
	}
	return 0;
}

int mld_list_grp_join(mld_t *mld, unsigned int iface, struct in6_addr *saddr)
{
#ifdef MLD_DEBUG
	char straddr[INET6_ADDRSTRLEN];
	char ifname[IF_NAMESIZE];
	unsigned ifx = mld->ifx[iface];
	inet_ntop(AF_INET6, saddr, straddr, INET6_ADDRSTRLEN);
	DEBUG("%s %s(%u): %s", __func__, if_indextoname(ifx, ifname), ifx, straddr);
#endif

#ifdef USE_LWMON
	if ((mld->log_ifnumber == 0 || mld->log_ifnumber == mld->ifx[iface]) && memcmp(&mld->log_addr->sin6_addr, saddr, 16) == 0)
		lwmon_log("server_multicast", "request");
#endif

	mld_grp_list_t *g, *p = NULL;
	for (g = mld->grps[iface];
		g && memcmp(&g->addr, saddr, sizeof(struct in6_addr));
		p = g, g = g->next);
	if (!g) { /* group not found, create entry */
		DEBUG("CREATE entry");
		g = calloc(1, sizeof (mld_grp_list_t));
		if (!g) return -1;
		memcpy(&g->addr, saddr, sizeof(struct in6_addr));
		if (p) p->next = g;
		else mld->grps[iface] = g;
		sem_post(&mld->sem_mld);
	}
	else DEBUG("UPDATING entry");
	/* update expiry */
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	ts.tv_sec += MLD_TIMEOUT;
	g->expires.tv_sec = ts.tv_sec;
	return 0;
}

int mld_list_grp_join_ai(mld_t *mld, unsigned int iface, struct addrinfo *ai)
{
	return mld_list_grp_join(mld, iface, aitoin6(ai));
}

void mld_list_grp_part(mld_t *mld, unsigned int iface, struct in6_addr *saddr)
{
	mld_grp_list_t *g;
	for (g = mld->grps[iface];
		g && memcmp(&g->addr, saddr, sizeof(struct in6_addr));
		g = g->next);
	if (!g) return; /* group not found - ignore */

#ifdef USE_LWMON
	if ((mld->log_ifnumber == 0 || mld->log_ifnumber == mld->ifx[iface]) && memcmp(&mld->log_addr->sin6_addr, saddr, 16) == 0)
		lwmon_log("server_multicast", "done");
#endif

	/* set expiry for group to Query Response Interval */
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	ts.tv_sec += MLD2_QRI / 1000;
	if (ts.tv_sec < g->expires.tv_sec) g->expires.tv_sec = ts.tv_sec;

	return mld_query_send(mld, iface, saddr);
}

void mld_list_grp_part_ai(mld_t *mld, unsigned int iface, struct addrinfo *ai)
{
	return mld_list_grp_part(mld, iface, aitoin6(ai));
}

/* check if addr matches an address on any of our interfaces
 * return nonzero if true (matched), 0 if not found */
int mld_thatsme(struct in6_addr *addr)
{
	int ret = 0;
	struct ifaddrs *ifaddr, *ifa;
	if (!addr) return ret;
	getifaddrs(&ifaddr);
	for (ifa = ifaddr; ifa; ifa = ifa->ifa_next) {
		if (!ifa->ifa_addr) continue;
		if (ifa->ifa_addr->sa_family != AF_INET6) continue;
		if (!memcmp(ifa->ifa_addr, addr, sizeof (struct in6_addr))) {
			ret = -1;
			break;
		}
	}
	freeifaddrs(ifaddr);
	return ret;
}

void mld_address_record(mld_t *mld, unsigned int iface, mld_addr_rec_t *rec)
{
	struct in6_addr grp = rec->addr;
	struct in6_addr *src = rec->src;
	int idx = -1;
	switch (rec->type) {
		case MODE_IS_INCLUDE:
		case CHANGE_TO_INCLUDE_MODE:
			if (!rec->srcs) {
#if USE_MLD_FILTER
				mld_filter_grp_del(mld, iface, &grp);
#else
				mld_list_grp_part(mld, iface, &grp);
#endif
				break;
			}
			for (int i = 0; i < rec->srcs; i++) {
				if (mld_thatsme(&src[i])) {
					idx = i;
					break;
				}
			}
			if (idx < 0) break;
			/* fallthru */
		case MODE_IS_EXCLUDE:
		case CHANGE_TO_EXCLUDE_MODE:
#if USE_MLD_FILTER
			mld_filter_grp_add(mld, iface, &grp);
#else
			mld_list_grp_join(mld, iface, &grp);
#endif
			break;
	}
}

void mld_listen_report(mld_t *mld, struct msghdr *msg)
{
	struct icmp6_hdr *icmpv6 = msg->msg_iov[0].iov_base;
	mld_addr_rec_t *mrec = msg->msg_iov[1].iov_base;
	unsigned int iface = mld_idx_iface(mld, interface_index(msg));
	uint16_t recs = ntohs(icmpv6->icmp6_data16[1]);
	DEBUG("MLD listen report with %u records received", recs);
	while (recs--) mld_address_record(mld, iface, mrec++);
}

void mld_msg_handle(mld_t *mld, struct msghdr *msg)
{
	struct icmp6_hdr *icmpv6 = msg->msg_iov[0].iov_base;
	if (icmpv6->icmp6_type == MLD2_LISTEN_REPORT) {
		mld_listen_report(mld, msg);
	}
}

int mld_listen(mld_t *mld)
{
	struct iovec iov[2] = {0};
	struct icmp6_hdr icmpv6 = {0};
	struct msghdr msg = {0};
	struct pollfd fds = { .fd = mld->sock, .events = POLL_IN };
	char ctrl[CMSG_SPACE(sizeof(struct in6_pktinfo))];
	char buf_name[IPV6_BYTES];
	char mrec[1500]; // FIXME - MTU size?
	int rc = 0;
	assert(mld);
	if (!mld->sock) {
		errno = ENOTSOCK;
		return -1;
	}
	iov[0].iov_base = &icmpv6;
	iov[0].iov_len = sizeof icmpv6;
	iov[1].iov_base = mrec;
	iov[1].iov_len = sizeof mrec;
	msg.msg_name = buf_name;
	msg.msg_namelen = IPV6_BYTES;
	msg.msg_control = ctrl;
	msg.msg_controllen = sizeof ctrl;
	msg.msg_iov = iov;
	msg.msg_iovlen = 2;
	msg.msg_flags = 0;
	DEBUG("MLD listener ready");
	assert(mld->cont);
	while (*(mld->cont)) {
		while (!(rc = poll(&fds, 1, 1000)) && *(mld->cont));
		if (rc == -1) {
			perror("poll()");
			return -1;
		}
		if (*(mld->cont)) {
			if ((recvmsg(mld->sock, &msg, 0)) == -1) {
				perror("recvmsg()");
				return -1;
			}
			mld_msg_handle(mld, &msg);
		}
	}
	return 0;
}

static void *mld_listen_job(void *arg)
{
	assert(arg);
	mld_t *mld = *(mld_t **)arg;
	mld_listen(mld);
	return arg;
}

lc_ctx_t *mld_lctx(mld_t *mld)
{
	return mld->lctx;
}

unsigned int mld_ifaces(mld_t *mld)
{
	return mld->len;
}

mld_t *mld_init(int ifaces)
{
	mld_t *mld = calloc(1, sizeof(mld_t) + ifaces * sizeof(mld_grp_list_t *));
	if (!mld) return NULL;
	mld->len = ifaces;
	mld->cont = &cont;
	/* create FIFO queue with timer writer thread + ticker + MLD listener */
	mld->timerq = job_queue_create(3);
	sem_init(&mld->sem_mld, 0, 0);
	if (!(mld->lctx = lc_ctx_new())) {
		ERROR("Failed to create librecast context");
		free(mld);
		mld = NULL;
	}
	return mld;
}

mld_t *mld_start(volatile int *cont, unsigned int iface, const struct sockaddr_in6 *addr)
{
	mld_t *mld = NULL;
	struct icmp6_filter filter;
	struct sockaddr_in6 *llocal;
	struct ifaddrs *ifaddr = NULL;
	struct ipv6_mreq req = {0};
	const int opt = 1;
	unsigned int ifx[IFACE_MAX] = {0};
	unsigned int idx;
	int joins = 0;
	int sock = socket(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6);
	if (sock == -1) {
		perror("socket()");
		return NULL;
	}
	if (setsockopt(sock, IPPROTO_IPV6, IPV6_RECVPKTINFO, &opt, sizeof(opt))) {
		perror("setsockopt()");
		goto exit_err_0;
	}

	/* Filter everything except MLDv2 Listen Reports */
	ICMP6_FILTER_SETBLOCKALL(&filter);
	ICMP6_FILTER_SETPASS(MLD2_LISTEN_REPORT, &filter);
	if (setsockopt(sock, IPPROTO_ICMPV6, ICMP6_FILTER, &filter, sizeof(filter)) == -1) {
		perror("setsockopt: ICMP6_FILTER");
		ERROR("Unable to set icmp6 filter");
	}

	if (inet_pton(AF_INET6, MLD2_CAPABLE_ROUTERS, &(req.ipv6mr_multiaddr)) != 1) {
		perror("inet_pton()");
		goto exit_err_0;
	}
	if (getifaddrs(&ifaddr)) {
		perror("getifaddrs()");
		goto exit_err_0;
	}
	/* join MLD2 ROUTERS group on all IPv6 multicast-capable interfaces */
	for (struct ifaddrs *ifa = ifaddr; ifa; ifa = ifa->ifa_next) {
		idx = if_nametoindex(ifa->ifa_name);
		DEBUG("trying %s (%u)", ifa->ifa_name, idx);
		if (iface && iface != idx) {
			DEBUG(" - wrong iface");
			continue;
		}
		if (ifa->ifa_addr->sa_family !=AF_INET6) {
			DEBUG(" - not IPv6");
			continue;
		}
		llocal = ((struct sockaddr_in6 *)ifa->ifa_addr);
		if (!IN6_IS_ADDR_LINKLOCAL(&llocal->sin6_addr)) {
			DEBUG("not link-local");
			continue;
		}
		if ((ifa->ifa_flags & IFF_MULTICAST) != IFF_MULTICAST) {
			DEBUG(" - not multicast capable");
			continue;
		}
		fputc('\n', stderr);
		req.ipv6mr_interface = idx;
		if (bind(sock, (struct sockaddr *)llocal, sizeof(struct sockaddr_in6)) == -1) {
			perror("bind");
		}
		if (!setsockopt(sock, IPPROTO_IPV6, IPV6_ADD_MEMBERSHIP, &req, sizeof(req))) {
			DEBUG("MLD listening on interface %s (%u)", ifa->ifa_name, idx);
			if (!idx) perror("if_nametoindex()");
			ifx[joins++] = idx;
			if (iface) break;
		}
		else {
			perror("setsockopt");
			DEBUG("failed to join on %s (%u)", ifa->ifa_name, idx);
		}
	}
	freeifaddrs(ifaddr);
	if (!joins) {
		DEBUG("Unable to join on any interfaces");
		goto exit_err_0;
	}
	DEBUG("%s() listening on %i interfaces", __func__, joins);
	mld = mld_init(joins);
	if (!mld) goto exit_err_0;
	if (cont) mld->cont = cont;
	memcpy(mld->ifx, ifx, sizeof ifx);
	mld->sock = sock;
	mld->log_ifnumber = iface;
	mld->log_addr = addr;
	job_push_new(mld->timerq, &mld_listen_job, &mld, sizeof mld, &free, JOB_COPY|JOB_FREE);
	return mld;
exit_err_0:
	close(sock);
	return NULL;
}
