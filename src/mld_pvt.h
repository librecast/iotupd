/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2022 Brett Sheffield <bacs@librecast.net> */

#ifndef MLD_PVT_H
#define MLD_PVT_H 1

#include "mld.h"
#include "job.h"
#include <assert.h>
#include <asm/byteorder.h>

#define MLD_DEBUG 1
#define BUFSIZE 1500
#define IFACE_MAX UCHAR_MAX

typedef enum {
	FILTER_MODE_INCLUDE = 1,
	FILTER_MODE_EXCLUDE,
} mld_mode_t;

struct mld_grp_list_s {
	mld_grp_list_t *next;
	struct in6_addr addr;    /* Multicast Address */
	struct timespec expires; /* record expires */
};

struct mld_s {
	lc_ctx_t *lctx;
	/* stop if cont points to zero value */
	volatile int *cont;
	job_queue_t *timerq;
	/* raw socket for MLD snooping */
	int sock;
	/* number of interfaces allocated */
	int len;
	/* iface -> interface_idx mapping */
	unsigned int ifx[IFACE_MAX];
	/* interface and address for which we log join/part events */
	unsigned int log_ifnumber;
	const struct sockaddr_in6 *log_addr;
	sem_t sem_mld; /* temporary hack - post when new mld address added */
	/* variable-length array of filters */
	mld_grp_list_t *grps[];
};

/* Multicast Address Record */
struct mld_addr_rec_s {
	uint8_t         type;    /* Record Type */
	uint8_t         auxl;    /* Aux Data Len */
	uint16_t        srcs;    /* Number of Sources */
	struct in6_addr addr;    /* Multicast Address */
	struct in6_addr src[];   /* Source Address */
};
#ifdef static_assert
static_assert(sizeof(struct mld_addr_rec_s) == 20, "ensure struct doesn't need packing");
#endif

/* Multicast Listener Query Message */
struct mld_query_msg_s {
	uint8_t         type;    /* type = 130 */
	uint8_t         code;
	uint16_t        checksum;
	uint16_t        mrc;     /* Maximum Response Code */
	uint16_t        res1;    /* Reserved */
	struct in6_addr addr;    /* Multicast Address */
#if defined(__LITTLE_ENDIAN_BITFIELD)
	uint8_t         qrv:3,   /* Querier's Robustness Variable */
			supp:1,  /* S Flag (Suppress Router-Side Processing) */
			res2:4;  /* Reserved */
#elif defined(__BIG_ENDIAN_BITFIELD)
	uint8_t         res2:4,  /* Reserved */
			supp:1,  /* S Flag (Suppress Router-Side Processing) */
			qrv:3;   /* Querier's Robustness Variable */
#else
#error "Please fix <asm/byteorder.h>"
#endif
	uint8_t         qqic;    /* Querier's Query Interval Code */
	uint16_t        srcs;    /* Number of Sources */

};

#endif /* MLD_PVT_H */
